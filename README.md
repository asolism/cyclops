# cyclops

Written for Statistics Canada

Andres Solis Montero, Feb 2020

# TODO

## Repo is too large
- Make the repository smaller than 2GB. 
- Models and large data should be stored in object-storage or
  at least a separate folder.
